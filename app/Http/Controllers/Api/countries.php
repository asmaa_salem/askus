<?php 

namespace App\Http\Controllers\API;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Country;


class countries extends Controller


{

  public function getCountries(Request $request)
  {

	 try{

          $lang =$request['lang'];
           if(!$lang)
      {
        return response()->json(["status"=>400]);
     
      }
      else
      {

      	 $countries_names= DB::table('countries')
         ->get();


         if($lang=='ar')
         {
           
           foreach ($countries_names as $countries_name) {
           $_country['country']=$countries_name->name_ar;
           $_country['cities']= DB::table('countries')
           ->join('cities','countries.id','cities.country_id')
           ->where('countries.id',$countries_name->id)
          ->select('cities.id as cityId','cities.name_ar as city')
          ->get();
            $countries[]=$_country;
            }

                    if (count($countries)==0) 
          {  
            
          	       return response()->json(["status"=>204]);

          }
          else
          {
          	return response()->json(["status"=>200,"countries"=>$countries]);
          }

         }
    
       elseif ($lang=='en') {

        foreach ($countries_names as $countries_name) {
           $_country['country']=$countries_name->name_en;
           $_country['cities']= DB::table('countries')
           ->join('cities','countries.id','cities.country_id')
           ->where('countries.id',$countries_name->id)
          ->select('cities.id as cityId','cities.name_en as city')
          ->get();
            $countries[]=$_country;
            }

                    if (count($countries)==0) 
          {  
            
                   return response()->json(["status"=>204]);

          }
          else
          {
            return response()->json(["status"=>200,"countries"=>$countries]);
          }
                }
       

      }
    

  	 	 }
   catch(\Exception $e)
     {
          return response()->json(['status' =>404]);
     }
  }


}
