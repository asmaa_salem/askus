<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('getCountries', 'API\countries@getCountries');
Route::post('register', 'API\register@register');
Route::post('validateCode','API\validateCode@validateCode');
Route::post('forgetPassword','API\forgetPassword@forgetPassword');
Route::post('changePassword','API\changePassword@changePassword');
Route::post('login','API\login@login');
Route::post('updateProfile','API\updateProfile@updateProfile');
Route::post('updatePhone','API\updatePhone@updatePhone');
