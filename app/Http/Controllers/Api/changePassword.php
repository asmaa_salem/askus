<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;
use Exception;
use Carbon\Carbon;
use App\User;
use Session;

class changePassword extends Controller
{
	public function changePassword(Request $request)
	{
	try{
	$validator=Validator::make($request->all(),[
 	'tmpToken'=>'required|max:68',
 	'newPassword'=>'required|min:5'
   ]);
	   $newPassword=$request['newPassword'];
	   $tmpToken = $request['tmpToken'];
	  if ($validator->fails())
        {
        return response()->json(["status"=>400,'Error'=>$validator->errors()->all()]);
        }

        else
          
        { 
        	
        	   
        	 if ($request->session()->has($request['tmpToken']))
        	  {
        	
        	 	$phone = $request->session()->get($request['tmpToken']);
                $user = User::where('phone',$phone)->first();
                  if($user->is_active ==0) {
                   return response()->json(["status"=>401]);
                    }


                 else{
                $user->password = Hash::make($newPassword);
                $user->save();

                  
        	 	return response()->json(["status"=>200]);
        	 }
        	}

        	 	else
        	 	{
                      return response()->json(["status"=>400]);
        	 	}


        	 	   
        }
         }
       catch(Exception $e) {
            return response()->json(['status' =>404]);
      } 

}

}