<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;
use Exception;

use App\User;
use Session;

/**
* 
*/
class updatePhone extends Controller
{
	
	public function updatePhone(Request $request)
	{

	$validator=Validator::make($request->all(),[
    'apiToken'=>'required|max:68',
     'phone'=>'min:6|numeric',
   ]);
        

         $apiToken = $request['apiToken'];
        $phone = $request['phone'];
        $user= User::where('apitoken',$apiToken)->first();
       

        if ($validator->fails())
        {
        return response()->json(["status"=>400,'Error'=>$validator->errors()->all()]);
        }

      elseif ( !$user ) {

      return response()->json(["status"=>403]);

      }
     
     elseif($user->is_active ==0) 
     {
                   return response()->json(["status"=>401]);
     }
    
     else
     {       
     	 $id = $user->id;
        $val = Validator::make($request->all(),[
    	'phone'=>'unique:users,phone,'.$id.'.id'
           ]);
        if ($val->fails()) 
     {
     	 return response()->json(["status"=>406]);
     }
     else{
             $code = str_random(6);
              Session::put($apiToken,['phone'=>$phone,'code'=>$code]);
              $arr= $request->session()->get($apiToken);
              return response()->json(["status"=>200,$arr]);
          }

     }
         

	}
}