<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Exception;
use Carbon\Carbon;
use App\User;
use Session;

class validateCode extends Controller
{
 public function validateCode(Request $request)

{ 

   $validator=Validator::make($request->all(),[
 	'phone'=>'min:6|numeric',
 	'code'=>'min:6',
 	'email'=>'email|min:5',
 	'apiToken'=>'string|max:68'
   ]);

   if ($validator->fails())
        {
        return response()->json(["status"=>400,'Error'=>$validator->errors()->all()]);
        }

        else
        {
		    $phone = $request['phone'];
		    $code= $request['code'];
		    $email = $request['email'];
		    $apiToken= $request['apiToken'];
		    if ( $phone && $code)

		    {
			    $user=User::where('phone',$phone)->first();

			    $apiToken1= $user->apitoken;
			    if (!$user) {
			    	return response()->json(["status"=>403]);
			    }
			    else{
			    $code_session= $request->session()->get('phone'.$phone);
			    $code_test = $code_session['code'];
			    if ($code===$code_test) 

			    {
			      $user->is_verified = 1;
			      $user->save();
			      return response()->json(["status"=>200,"apiToken"=>$apiToken1]);
			    }
			     
			     else
			     {
			     	return response()->json(["status"=>407]);
			     }
              }
		    }

		    elseif ($email && $code) 
		    {

		    $user=User::where('email',$email)->first();
		    if (!$user) 
		    {
	        return response()->json(["status"=>403]);
			 
		    }
		    else{
	        $phone = $user->phone;
	         $session= $request->session()->get('phone'.$phone);
	         $code_test = $session['code'];
	         if ($code===$code_test) 
	         {
	           $tmpToken=str_random(64);
	           /* $tempApi_session= $request->session()->get('phone'.$phone);
	             $tempApi_session['tmpToken']= $tempApi;
	              Session::put('phone'.$phone, $tempApi_session);
	              $tmpToken= $tempApi_session['tmpToken'];*/
              //    $tempApi_session= $request->session()->get('phone'.$phone);
           //  $tempApi_session['tmpToken']= $tempApi;
             // Session::put('phone'.$phone, $tempApi_session);
             
             Session::put($tmpToken,$phone);
             //$tempApi=$request->session()->has($tmpToken);
            // $t = $request->session()->get($tempApi);

             // $tmpToken= $tempApi_session['tmpToken'];
	         	return response()->json(["status"=>200,"tmpToken"=>$tmpToken]);
	          }
	    
	       else
	     {
	     	return response()->json(["status"=>407]);
	     }
		    }
		}
		    elseif ($apiToken && $code)

		   {
             $session_phone= $request->session()->get($apiToken);
             $v_code =  $session_phone['code'];

             if ($code ==$v_code) 
             {
             	 $user=User::where('apitoken',$apiToken)->first();
             	 if (!$user) 
             	 {
             	 	 return response()->json(["status"=>403]);
             	 }
             	 else{
             	 $user->phone= $session_phone['phone'];
             	 $user->save();
             	 return response()->json(["status"=>200]);
             	}
             }

              else

              {
              	return response()->json(["status"=>407]);
              }

		    	
		    }



        }

}

}
