<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Validator;
use Exception;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\User;
use Session;

class login extends Controller
{
	
	public function login(Request $request)
	{
		try{
		$validator=Validator::make($request->all(),[
		
        'password'=>'required|min:5',
        'phone'=>'required|min:6|numeric',

        
  ]);

		 if ($validator->fails())
        {
        return response()->json(["status"=>400,'Error'=>$validator->errors()->all()]);
        }

        else
        {    $phone=$request['phone'];
        	  $password =$request['password'];
        	  $user =  $user=User::where('phone',$phone)->first();
        	                  if (!$user)
                {
               	  return response()->json(["status"=>401]);
               }
               elseif ((Hash::check($password,$user->password)== false))
                {
               	 return response()->json(["status"=>401]);
               }
               elseif($user->is_active ==0) {
                   return response()->json(["status"=>401]);
                    }

               elseif ($user->is_verified == 0) 
               {
               	$code= str_random(6);
               	$data = Array( 'phone' => $phone,'code'=>$code) ;
                  /*$request->session()->put(['user',$data]);
*/
                  Session::put('phone'.$phone, $data);
                  // $request->session()->put($email ,$code ,$time);

                  //$request->session()->put(['phone' => $email , 'code'=>$code , "time"=>$time]);
                 $code2 = $request->session()->get('phone'.$phone);
                return response()->json(["status"=>409]);
               }

               else

               {

                     $user2= DB::table('users')
                    ->join('cities','users.city_id','cities.id')
                    ->join('countries','cities.country_id','countries.id')
                    ->where('users.phone',$phone)
                    ->select('users.apitoken as apiToken','users.name as name','users.email as email','countries.id as countryId','users.city_id as cityId','users.photo_url as photo' )->get();
                     return response()->json(["status"=>200,$user2]);

               }

        }
}
    catch(\Exception $e)
     {
          return response()->json(['status' =>404]);
     }
	
}}