<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;
use Exception;


use App\User;


class updateProfile extends Controller
{
   
	public function updateProfile(Request $request)
	{
		try{
	

    $validator=Validator::make($request->all(),[
    'apiToken'=>'required|max:68',
    'name'=>'string',
 	'email'=>'email|min:5',
 	'cityId'=>'integer',
 	'photo'=>'image',
 	'password'=>'min:5|string'
   ]);
   
      $apiToken = $request['apiToken'];
      $name = $request['name'];
      $cityId = $request['cityId'];
      $email=$request['email'];
      $photo = $request['photo'];
      $password = $request['password'];

      $user = User::where('apitoken',$apiToken)->first();
      $id = $user->id;
        $val = Validator::make($request->all(),[
    	'email'=>'unique:users,email,'.$id.'.id'
]);
     if ($validator->fails())
        {
        return response()->json(["status"=>400,'Error'=>$validator->errors()->all()]);
        }
      elseif ( !$user ) {
      return response()->json(["status"=>403]);
      }
     elseif($user->is_active ==0) 
     {
                   return response()->json(["status"=>401]);
     }
        else
        {

        	
        	if ($name)
         {
        	$user->name= $name;
            $user->save();
        }

        if ($email) {

        	
            if ($val->fails()) 
            {
            	 return response()->json(["status"=>405]);
             
           }
           else{
               $user->email = $email;
               $user->save();
               }
        	
        }
          
        if ($cityId)

           {
          	$user->city_id = $cityId;
            $user->save();
          	}

         if ($password) 
         {
            	$user->password = Hash::make($password);
            	$user->save();
            }
           if ($photo)
            {

        $extension=$photo->getClientOriginalExtension();
        $imageName =$photo->getFilename().'.'.$extension;
        $success=Storage::disk('public')->put("/ads/".$imageName, File::get($img));
        $user->photo= $imageName;
        $user->save();

            }

      


        }
          return response()->json(["status"=>200,$user]);

 }
       catch(Exception $e) {
            return response()->json(['status' =>404]);
      } 

        }


	
}