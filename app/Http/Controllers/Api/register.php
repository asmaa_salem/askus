<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;
use Exception;
use Carbon\Carbon;
use App\User;
use Session;

class register extends Controller
{
 
 
	public function register(Request $request)
	{
		try{

		 $validator=Validator::make($request->all(),[
		
        'name'=>'required|alpha',
        'password'=>'required|min:5',
        'email'=>'required|email|min:5',
        'phone'=>'required|min:6|numeric',
        'cityId'=>'required|integer'
  ]);

		 
          $name=$request['name'];
          $password=$request['password'];
          $phone=$request['phone'];
          $email=$request['email'];
          $cityId=$request['cityId'];
         

        if ($validator->fails())
        {
        return response()->json(["status"=>400,'Error'=>$validator->errors()->all()]);
        }

        else

        {

          $user_email_ex = User::where('email',$email)->get();
          $user_phone_ex = User::where('phone',$phone)->get();
          if (count($user_email_ex)>0 ) 
          {
          	return response()->json(["status"=>405]);
          }
        elseif (count($user_phone_ex)>0 ) 
        {
        		return response()->json(["status"=>406]);
        }
         
         else
         {
         	

               /*   $code2 = $request->session()->get('code');*/
                  $user = new User();
                  $user->name = $name;
                  $user->email =$email;
                  $user->phone =$phone;
                  $user->city_id=$cityId;
                  $user->password= Hash::make($password);
                  $user->apitoken=str_random(64);
                  $user->created_at =Carbon::now();
                  $user->save();  
                 
                  $code=str_random(6);

                  $data = Array( 'phone' => $phone,'code'=>$code) ;
                  /*$request->session()->put(['user',$data]);
*/
                  Session::put('phone'.$phone, $data);
                  // $request->session()->put($email ,$code ,$time);

                  //$request->session()->put(['phone' => $email , 'code'=>$code , "time"=>$time]);
                 $code2 = $request->session()->get('phone'.$phone);
                 /* session_start(); 
                $_SESSION['phone'] = $phone;
                $_SESSION['code'] = str_random(4);
                 $_SESSION['time'] = Carbon::now();
                 $code2[]= $_SESSION;*/
                
                   return response()->json(["status"=>200,$code2]);                 
                  
               
            
         
        }


	}
}
	
	 catch(\Exception $e)
     {
          return response()->json(['status' =>404]);
     }
}
}
