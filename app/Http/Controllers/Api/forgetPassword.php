<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Exception;
use Carbon\Carbon;
use App\User;
use Session;

class forgetPassword extends Controller
{
     
  public function forgetPassword(Request $request)
  {
    try{
  	 $validator=Validator::make($request->all(),[
        'email'=>'required|email|min:5',
    
       
  ]);
        $email = $request['email'];
  	   if ($validator->fails())
        {
        return response()->json(["status"=>400,'Error'=>$validator->errors()->all()]);
        }

        else

        {
              $user=User::where('email',$email)->first();

              if (!$user) {
              	return response()->json(["status"=>408]);
               }

               else
               {
                   if ($user->is_active = 0) {
                   return response()->json(["status"=>401]);
                    }
                    else
                    {
                  $phone = $user->phone;
                  $code=str_random(6);

                  $data = Array( 'phone' => $phone,'code'=>$code) ;
                 
                  Session::put('phone'.$phone, $data);
                  $code2 = $request->session()->get('phone'.$phone);

                   return response()->json(["status"=>200,"code"=>$code]);
                    }
               }
        }
         }
       catch(Exception $e) {
            return response()->json(['status' =>404]);
      } 

  }

}